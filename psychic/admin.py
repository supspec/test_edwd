from django.contrib import admin
from . import models


@admin.register(models.Psychic)
class PsychicAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'rate')

admin.site.register(models.PsychicEvent)
