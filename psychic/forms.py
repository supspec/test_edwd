from django import forms


class ExamForm(forms.Form):
    number = forms.IntegerField(label='Number', max_value=99, min_value=1, required=True)
    conclusions = forms.CharField(widget=forms.HiddenInput(), required=True)
