from django.core.management.base import BaseCommand
import names
from psychic.models import Psychic


class Command(BaseCommand):

    def handle(self, *args, **options):

        for _ in range(10):
            Psychic.objects.create(name=names.get_full_name())
        print('Generated 10 psychics.')
