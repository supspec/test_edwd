from django.db import models
import random


class Psychic(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name_plural = "Psychis"

    @property
    def conclusion(self):
        return random.randint(1, 99)

    @property
    def rate(self):
        count = self.events.all().count()
        if count:
            right = 0
            for event in self.events.all():
                if event.right:
                    right += 1
            return round((right / count) * 100)
        return 0

    def __str__(self):
        return self.name


class PsychicEvent(models.Model):
    psychic = models.ForeignKey(Psychic, on_delete=models.CASCADE, related_name='events')
    answer = models.PositiveSmallIntegerField()
    right_answer = models.PositiveSmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def right(self):
        return self.answer == self.right_answer

    def __str__(self):
        return f"{self.psychic.name}: {self.answer} ({self.right_answer}) ({self.right})"
