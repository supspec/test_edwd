from . import models
import random


def get_physics(at_least=2):
    psychic_count = models.Psychic.objects.all().count()
    if psychic_count >= at_least:
        psychic_count = 5 if psychic_count > 5 else psychic_count
        limit = random.randint(at_least, psychic_count)
        psychic = models.Psychic.objects.order_by('?')[:limit]
        return psychic
    raise RuntimeError(f'Не достаточно экстрасенсов '
                       f'(нужно как минимум {at_least}), '
                       f'попросите админа добавить еще!')


def save_conclusions(number: int, conclusion_string: str) -> None:
    conclusions = conclusion_string.split(":")
    for conclusion in conclusions:
        pk, num = conclusion.split(";")
        psychic = models.Psychic.objects.get(pk=pk)
        models.PsychicEvent.objects.create(psychic=psychic, right_answer=number, answer=num)
