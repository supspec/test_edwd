from django.shortcuts import render, redirect
from . import models
from . import forms
from .utils import get_physics, save_conclusions


def home(request):
    psychics = models.Psychic.objects.prefetch_related('events').all()
    context = {'psychics': psychics}
    return render(request, 'index.html', context=context)


def exam(request):
    error_message = None
    if request.method == 'POST':
        form = forms.ExamForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            save_conclusions(int(data['number']), data['conclusions'])
            return redirect('home')
        error_message = form.errors
    psychics_and_conclusions = [(psychic, psychic.conclusion) for psychic in get_physics()]
    conclusion_string = ":".join(f"{item[0].id};{item[1]}" for item in psychics_and_conclusions)
    context = {
        'form': forms.ExamForm,
        'psychics': psychics_and_conclusions,
        'conclusion_string': conclusion_string,
        'error_message': error_message
    }
    return render(request, 'exam.html', context=context)
